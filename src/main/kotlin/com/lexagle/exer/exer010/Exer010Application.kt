package com.lexagle.exer.exer010

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Exer010Application

fun main(args: Array<String>) {
	runApplication<Exer010Application>(*args)
}
